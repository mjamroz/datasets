#!/usr/bin/env python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree

from bs4 import BeautifulSoup
def f1():
    with open("out.txt", "w") as fw:
        soup = BeautifulSoup(open("kur.xml"), from_encoding='UTF-8')
        for drug in soup.find_all('drug', type="small molecule"):
            inchikey = ''
            smiles = ''
            water_sol = ''
            pka = ''
            calc = drug.find('calculated-properties')
            for c in calc.find_all('kind'):
                if c.string == 'InChIKey':
                    inchikey = c.parent.value.string
                elif c.string == 'SMILES':
                    smiles = c.parent.value.string

            for kind in drug.find_all('kind'):
                if kind.parent.parent.name == 'experimental-properties':
                    if kind.string == 'Water Solubility':
                        water_sol = kind.parent.value.string
                    elif kind.string == 'pKa':
                        pka = kind.parent.value.string

            did = drug.find('drugbank-id', {'primary': "true"}).string
            string = u"\tDLMTD\t".join([" %s "  for i in range(4)])
            string = string+'\n'
            if water_sol != '' or pka != '':
                fw.write(string % (inchikey.replace('InChIKey=', ''), water_sol, pka, did))
f1()
